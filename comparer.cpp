#include "comparer.h"

Comparer::Comparer() {
}

Comparer::Comparer( string col ) {
    this->orderCol = col;
}

bool Comparer::operator()(const individual first, const individual second) {
    if(orderCol == "name") {
        return first.name < second.name;
    } else if(orderCol == "birth") {
        return first.yearOfBirth < second.yearOfBirth;
    } else if(orderCol == "death") {
        return first.yearOfDeath < second.yearOfDeath;
    } else if(orderCol == "sex") {
    return first.sex < second.sex;
}
    throw; // Exception()
}

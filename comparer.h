#ifndef COMPARER_H
#define COMPARER_H

#include <iostream>
#include "individual.h"
using namespace std;

class Comparer {
private:
    string orderCol;
    bool compare(std::string first, std::string second);
public:
    Comparer();
    Comparer( std::string col);
    bool operator()(const individual first, const individual second);

};
#endif // COMPARER_H

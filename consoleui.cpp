#include "consoleui.h"
#include "service.h"
#include "comparer.h"
consoleUI::consoleUI()
{

}

void consoleUI::start(){
string action;

    do{//let the user choose which action they would like to perform.
    cout << "Choose which action you would like to perform: add, sort, list, search, quit" << endl;
            cin >> action;

        if(action=="add"){

            add();

        }
        else if(action=="sort"){

             uisort(); //new

        }else if(action=="list"){
            list();
        }else if(action=="search"){
            search();

        }else if(action=="quit"){

            return;
        }
        else{
            cout << "Invalid action please try again!";


        }
    }while(action!="quit");

}

void consoleUI::add(){//the interface for adding a new user.
    string n;
    int yb, yd;
    char s;
    bool valid = false;

    do{
        cout << "Enter name:" << endl;
        cin.ignore(100,'\n');
        getline(cin,n);
        valid = true; //serv.validate(n);
        if(!valid) cout << "Invalid input! Please try again" << endl;
    }while(!valid);

    do{
        cout << "Enter year of birth:" << endl;
        cin >> yb;
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
        valid = serv.validate(yb);
        if(!valid) cout << "Invalid input! Please try again" << endl;

    }while(!valid);

    do{
    cout << "Enter year of death:" << endl;
        cin >> yd;
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
        valid = serv.validate(yd);
        if(!valid) cout << "Invalid input! Please try again" << endl;
    }while(!valid);

    do{
        cout << "Enter sex (The options are \'m\' for male or \'f\' for female):" << endl;
        cin >> s;
        valid = serv.validate(s);
        if(!valid) cout << "Invalid input! Please try again" << endl;
    }while(!valid);


    serv.add(n ,yb ,yd ,s);

}
void consoleUI::list(){//display the data that has been gathered by the program.

    cout << setw(30)<< left << "Name" << setw(20)<< left << "Year of Birth" << setw(20)<< left << "Year of Death" << setw(5)<< left<< "Sex" << endl;
    cout << string(80, '-') <<  endl;
    std::list<individual> listInd = serv.list();

    for(std::list<individual>::iterator iter = listInd.begin(); iter != listInd.end(); iter++){

        cout << *iter;

    }

    cout << endl;
    cout << string(80, '-') <<  endl;

}
void consoleUI::listsr(std::list<individual> sr){//display the search results.

    cout << setw(30)<< left << "Name" << setw(20)<< left << "Year of Birth" << setw(20)<< left << "Year of Death" << setw(5)<< left<< "Sex" << endl;
    cout << string(80, '-') <<  endl;

    for(std::list<individual>::iterator iter = sr.begin(); iter != sr.end(); iter++){

        cout << *iter;

    }

    cout << endl;
    cout << string(80, '-') <<  endl;

}
void consoleUI::search(){//the interface for searching through the data.
    string action;
    string paramstr;
    int paramInt;
    char paramSex;

    cout << "Please select which field you would like to search through!\n";
    cout << "The options are: 'name, 'birth, 'death' and 'sex'." << endl;
    cout << string(80, '-') <<  endl;
        cin >> action;

        if(action=="name"){
        cout << "Please enter the name you would like to searh for!" << endl;
            cin.ignore(100,'\n');
            getline(cin, paramstr);
            std::list<individual> result = serv.search(paramstr);

            listsr(result);

            return;

        }
        else if(action=="birth"){
        cout << "Please enter the year of birth you would like to searh for!" <<  endl;
            cin >> paramInt;
            std::list<individual> result = serv.searchYB(paramInt);

            listsr(result);

            return;

        }else if(action=="death"){
            cout << "Please enter the year of death you would like to searh for!" << endl;
                cin >> paramInt;
           std::list<individual> result = serv.searchYD(paramInt);

           listsr(result);

           return;


        }else if(action=="sex"){
            cout << "Please enter the sex you would like to searh for!" << endl;
            cin >> paramSex;
             std::list<individual> result = serv.search(paramSex);

             listsr(result);

             return;
        }
        else{
            cout << "Invalid action please try again!";
        }
}

void consoleUI::uisort(){ //new
    string sortfile;
    bool valid = false;

    do{
        cout << "Please select the field by which you would like to sort: \nname, birth, death, sex" << endl;
        cin >> sortfile;
        valid = serv.validsort(sortfile);
        if(!valid) cout << "Invalid input! Please try again" << endl;
    }while(!valid);

    Comparer comp = Comparer(sortfile);
    std::list<individual> sortedlist = serv.sort(comp);
    cout << string(80, '-') <<  endl;
    for(std::list<individual>::iterator iter = sortedlist.begin(); iter != sortedlist.end(); iter++){

            cout << *iter;

    }
    cout << string(80, '-') <<  endl;
}

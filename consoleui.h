#ifndef CONSOLEUI_H
#define CONSOLEUI_H

#include <iostream>
#include <string>
#include <list>
#include "service.h"
#include "individual.h"

#include<limits>

using namespace std;

class consoleUI
{
public:
    consoleUI();
    void start();
    void add();
    void list();
    void search();
    void listsr(std::list<individual> sr);
    void uisort();
private:
    service serv;
};

#endif // CONSOLEUI_H

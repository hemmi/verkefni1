#include "individual.h"

individual::individual()//default constructor
{
   name = "";
   yearOfBirth = 0;
   yearOfDeath = 0;
   sex =' ';

}

individual::individual(const string& n, const int& yb, const int& yd, const char& s)//constructor with four parameters one for each field;
{
    name = n;
    yearOfBirth = yb;
    yearOfDeath = yd;
    sex = s;

}
ostream& operator<<(ostream& out, individual ind){ //overloading the << operator.

    out << setw(30) << left << ind.name << setw(20) << left << ind.yearOfBirth << setw(20) << left << ind.yearOfDeath << setw(5)<< left << ind.sex << endl;

    return out;
}
string individual::getName(){

    return name;
}
int individual::getYB(){

    return yearOfBirth;
}
int individual::getYD(){

    return yearOfDeath;
}
char individual::getSex(){

    return sex;
}

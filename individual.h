#ifndef INDIVIDUAL_H
#define INDIVIDUAL_H
#include <iostream>
#include <string>
#include <iomanip>
#include <algorithm>


using namespace std;

class individual
{
public:
    individual();
    individual(const string& n, const int& yb, const int& yd, const char& s);
    friend ostream& operator <<(ostream& out, individual ind);

    string getName();
    int getYB();
    int getYD();
    char getSex();

    string name;
    int yearOfBirth;
    int yearOfDeath;
    char sex;
};

#endif // INDIVIDUAL_H

#include "repository.h"



repository::repository() //retrieve data form the data file.
{
    ifstream inFile ("data.txt");

    if(inFile.is_open()){
        string name;
        int yearB, yearD;
        char sex;
        individual ind = individual();
        while(!inFile.eof()){

            inFile >> name >> yearB >> yearD >> sex;

            for(unsigned int i=0; i<name.length();i++){

               if(name[i]=='*')name[i]=' '; //check '*' in name field and change it to ' ' to chance it back to its original value;

            }

            ind = individual(name, yearB, yearD, sex);
            vec.push_back(ind);
        }
        inFile.close();
    }vec.pop_back();
}
void repository::add(individual ind){//write data to the data file.

    string name;
    int yearb, yeard;
    char sex;

    vec.push_back(ind);

    ofstream outFile ("data.txt");

    if(outFile.is_open()){

        for(list<individual>::iterator iter = vec.begin(); iter != vec.end(); iter++){

            name = iter->getName();
            yearb = iter->getYB();
            yeard = iter->getYD();
            sex = iter->getSex();


            for(unsigned int i=0; i<name.length();i++){

               if(isspace(name[i]))name[i]='*'; //check for space in name field and change it to '*' because whitespace is used as a delimiter in the data file;

            }

            outFile << name << "\t" << yearb << "\t" << yeard << "\t" << sex << endl;
        }
        outFile.close();
    }
}

void repository::remove(){


}
list<individual> repository::getlist(){


   return vec;
}
std::list<individual> repository::sort(Comparer comp){

    vec.sort(comp);

    return vec;

}

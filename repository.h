#ifndef REPOSITORY_H
#define REPOSITORY_H

#include "individual.h"
#include <list>
#include <vector>
#include <fstream>
#include "comparer.h"

using namespace std;

class repository
{
public:
    repository();
    void remove();
    void add(individual ind);
    list<individual> getlist();
    std::list<individual> sort(Comparer comp);
private:
   list<individual> vec;
};

#endif // REPOSITORY_H

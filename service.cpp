#include "service.h"

service::service()
{
    repo = repository();
}

void service::add(const string& n, const int& yb, const int& yd, const char& s)//add a new name.
{

    individual ind(n,yb,yd,s);

    repo.add(ind);

}

void service::remove(){


}

list<individual> service::list(){//prepare the contents of the list for being printed on the user's screen.
    std::list<individual> vec = repo.getlist();

        return vec;
}
individual service::show(individual ind){

   return ind;
}

bool service::validate(string name){//validate the input to the name field.

    if(islower(name[0]))
        toupper(name[0]);

    for (string::iterator iter=name.begin(); iter!=name.end(); ++iter)
      {
        if (!isalpha(*iter))
        return false;
      }

    return true;
}
bool service::validate(int year){//validate the input to the year fields.

    return(year<=2014&&year>1799);

}
bool service::validate(char& sex){//validate the input to the sex fields.
    if(isupper(sex))
       sex = tolower(sex);
    return(sex=='m'||sex=='f');
}

list<individual> service::search(string input){//search through the name field.
    hit.clear();
    unsigned int found;
    bool temp;
    std::list<individual> vec = repo.getlist();
    string name;

    for(std::list<individual>::iterator iter = vec.begin(); iter != vec.end(); iter++){

        name = iter->getName();

        found = name.find(input);

        if (found!=std::string::npos){

            temp = true ;

        }else{
            temp = false;
        }
        hit.push_back(temp);
    }std::list<individual> sr = searchResult();

    return sr;
}

list<individual> service::searchYB(int input){//search through the year of birth field.
    hit.clear();
    bool temp;
    std::list<individual> vec = repo.getlist();

    int yearb;

    for(std::list<individual>::iterator iter = vec.begin(); iter != vec.end(); iter++){

        yearb = iter->getYB();

        if(input==yearb){

            temp = true ;

        }else{
            temp = false;
        }
        hit.push_back(temp);
    }
    std::list<individual> sr = searchResult();

    return sr;
}

std::list<individual> service::searchYD(int input){//search through the year of death field.
    hit.clear();
    bool temp;
    std::list<individual> vec = repo.getlist();

    int yeard;

    for(std::list<individual>::iterator iter = vec.begin(); iter != vec.end(); iter++){

        yeard = iter->getYD();

        if(input==yeard){

            temp = true ;

        }else{
            temp = false;
        }
        hit.push_back(temp);
    }
    std::list<individual> sr = searchResult();

    return sr;
}

list<individual> service::search(char input){//search through the sex field.
    hit.clear();
    bool temp;
    std::list<individual> vec = repo.getlist();

    char sex;

    for(std::list<individual>::iterator iter = vec.begin(); iter != vec.end(); iter++){

        sex = iter->getSex();
        if(input==sex){

            temp = true ;

        }else{
            temp = false;
        }
        hit.push_back(temp);
    } std::list<individual> sr = searchResult();
    return sr;
}

list<individual> service::searchResult(){//prepare the search results for output.

    std::list<individual> vec = repo.getlist();
    std::list<individual> searchList;
    individual temp;
    int i=0;
    for(std::list<individual>::iterator iter = vec.begin(); iter != vec.end(); iter++){

        if(hit[i]==true){
            temp = *iter;
            searchList.push_back(temp);
        }
        i++;
    }
return searchList;
}
bool service::validsort(string sortfile){ //new
    if(sortfile == "name"){
        return true;
    }
    else if(sortfile =="birth"){
        return true;
    }
    else if(sortfile =="death"){
        return true;
    }
    else if(sortfile =="sex"){
        return true;
    }
    else{
        return false;
    }
}

std::list<individual> service::sort(Comparer comp){

    std::list<individual> list2 = repo.sort(comp);

    return list2;

}

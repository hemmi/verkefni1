#ifndef SERVICE_H
#define SERVICE_H
#include "individual.h"
#include "repository.h"
#include "comparer.h"
#include <cctype>
#include <ctype.h>
#include <list>
#include <algorithm>

using namespace std;

class service
{
public:
    service();
    void remove();
    std::list<individual> list();
    bool validate(string name);
    bool validate(int year);
    bool validate(char& sex);
    void add(const string& n, const int& yb, const int& yd, const char& s);

    string fixName(string n);
    individual show(individual ind);

    std::list<individual> search(string input);
    std::list<individual> searchYB(int input);
    std::list<individual> searchYD(int input);
    std::list<individual> search(char input);

    std::list<individual> searchResult();
    bool validsort(string sortfile);

    std::list<individual> sort(Comparer comp);
private:
  repository repo;
  vector<bool> hit;
};

#endif // SERVICE_H

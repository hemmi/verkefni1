#-------------------------------------------------
#
# Project created by QtCreator 2014-11-28T16:02:54
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = verkefni1
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    consoleui.cpp \
    individual.cpp \
    repository.cpp \
    service.cpp \
    comparer.cpp

HEADERS += \
    consoleui.h \
    individual.h \
    repository.h \
    service.h \
    comparer.h
